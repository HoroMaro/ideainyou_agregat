<?php get_template_part('inc/frontend/footers/global-footer'); ?>

<?php $footerMenu = get_field('footer_menu_' . getLang(), 'option') ?>
<footer class="footer section fp-auto-height">
    <div class="marquee">
        <p class="text"><span><?= get_field('footer_slider_' . getLang(), 'option') ?></span></p>
    </div>
    <div class="footer-menu">
        <div class="block space-between">
            <div class="logo">
                <a href="<?= getSiteUrl() ?>"><?= get_template_part('/template-parts/part', 'logo') ?></a>
            </div>
            <p class="copyrights"><?= get_field('copyrights_en', 'option') ?></p>
        </div>
        <?php if (!empty($footerMenu)): ?>
        <div class="right">
            <?php foreach ($footerMenu as $footerContent): ?>
                <div class="block">
                    <div class="title"><?= $footerContent['menu_item_title'] ?></div>
                    <div class="items">
                        <?php foreach ($footerContent['menu_items'] as $item): ?>
                            <a href="<?= $item['item']['url'] ?>" target="<?= $item['item']['target'] ?>"
                               class="item"><?= $item['item']['title'] ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php endif; ?>
            <div class="block space">
                <?php foreach (get_field('socials', 'option') as $item): ?>
                    <a href="<?= $item['link']['url'] ?>" target="<?= $item['link']['target'] ?>" class="social">
                        <?= get_template_part('/template-parts/part', $item['choose_social'] . '-logo'); ?>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
        <p class="copyrights mobile"><?= get_field('copyrights_en', 'option') ?></p>
    </div>
</footer>

