jQuery(document).ready(function ($){
    var newsPrev = $('#news .prev');
    var newsNext = $('#news .next');
    var newsOwl = $('#news .owl-carousel');
    var photoOwl = $('.img-slider .owl-carousel');
    var photoPrev = $('.img-slider .prev');
    var photoNext = $('.img-slider .next');
    var trainOwl = $('#train .owl-carousel');
    var trainPrev = $('#train .prev');
    var trainNext = $('#train .next');
    var driversOwl = $('#drivers .owl-carousel');
    var driversPrev = $('#drivers .prev');
    var driversNext = $('#drivers .next');
    var passangersOwl = $('#passangers .owl-carousel');
    var passangersPrev = $('#passangers .prev');
    var passangersNext = $('#passangers .next');

    passangersOwl.owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        dots: false,
        responsive:{
            0:{
                items:1,
            },
        }
    });

    passangersPrev
        .on('mouseenter', function (){
            $('#passangers .line').addClass('left');
        })
        .on( "mouseleave", function(){
            $('#passangers .line').removeClass('left');
        })
        .on('click', function (){
            passangersOwl.trigger('prev.owl.carousel');
        })

    passangersNext
        .on('mouseenter', function (){
            $('#passangers .line').addClass('right');
        })
        .on( "mouseleave", function(){
            $('#passangers .line').removeClass('right');
        })
        .on('click', function (){
            passangersOwl.trigger('next.owl.carousel');
        })

    driversOwl.owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        dots: false,
        responsive:{
            0:{
                items:1,
                margin:0
            }
        }
    });

    driversPrev
        .on('mouseenter', function (){
            $('#drivers .line').addClass('left');
        })
        .on( "mouseleave", function(){
            $('#drivers .line').removeClass('left');
        })
        .on('click', function (){
            driversOwl.trigger('prev.owl.carousel');
        })

    driversNext
        .on('mouseenter', function (){
            $('#drivers .line').addClass('right');
        })
        .on( "mouseleave", function(){
            $('#drivers .line').removeClass('right');
        })
        .on('click', function (){
            driversOwl.trigger('next.owl.carousel');
        })

    trainOwl.owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        dots: false,
        responsive:{
            0:{
                items:1,
                margin:0
            }
        }
    });

    trainPrev
        .on('mouseenter', function (){
            $('#train .line').addClass('left');
        })
        .on( "mouseleave", function(){
            $('#train .line').removeClass('left');
        })
        .on('click', function (){
            trainOwl.trigger('prev.owl.carousel');
        })

    trainNext
        .on('mouseenter', function (){
            $('#train .line').addClass('right');
        })
        .on( "mouseleave", function(){
            $('#train .line').removeClass('right');
        })
        .on('click', function (){
            trainOwl.trigger('next.owl.carousel');
        })

    newsOwl.owlCarousel({
        loop:true,
        margin:5,
        nav:false,
        dots: false,
        responsive:{
            0:{
                items:1,
                margin:0
            },
            600:{
                items:2,
                margin:40
            },
            1200:{
                items:3
            }
        }
    });

    newsPrev
    .on('mouseenter', function (){
        $('#news .line').addClass('left');
    })
    .on( "mouseleave", function(){
        $('#news .line').removeClass('left');
    })
    .on('click', function (){
        newsOwl.trigger('prev.owl.carousel');
    })

    newsNext
    .on('mouseenter', function (){
        $('#news .line').addClass('right');
    })
    .on( "mouseleave", function(){
        $('#news .line').removeClass('right');
    })
    .on('click', function (){
        newsOwl.trigger('next.owl.carousel');
    })

    photoOwl.owlCarousel({
        loop:true,
        margin:5,
        nav:false,
        dots: false,
        responsive:{
            0:{
                items:1,
                margin:0
            }
        }
    });

    photoPrev
        .on('mouseenter', function (){
            $('.img-slider .line').addClass('left');
        })
        .on( "mouseleave", function(){
            $('.img-slider .line').removeClass('left');
        })
        .on('click', function (){
            photoOwl.trigger('prev.owl.carousel');
        })

    photoNext
        .on('mouseenter', function (){
            $('.img-slider .line').addClass('right');
        })
        .on( "mouseleave", function(){
            $('.img-slider .line').removeClass('right');
        })
        .on('click', function (){
            photoOwl.trigger('next.owl.carousel');
        })
})
