jQuery(document).ready(function ($){
    var currentTheme = $('.hero a.active').attr("data-theme");
    var menuItem = $('.hero .right nav a');
    var hero = $('section.hero');
    $('div.'+ currentTheme).css({'display': '-webkit-flex', 'display': '-ms-flexbox', 'display': 'flex'});
    $('div.description.'+ currentTheme).css({'display': '-webkit-flex', 'display': '-ms-flexbox', 'display': 'flex'});

    menuItem.on('click', function (e){
        e.preventDefault();
        menuItem.removeClass('active');
        $(this).addClass('active');
        hero.removeClass(currentTheme);
        currentTheme = $('.hero a.active').attr("data-theme");
        $('.hero .main-content, .hero .description').css('display', 'none');
        hero.addClass(currentTheme);
        $('div.'+ currentTheme).css({'display': '-webkit-flex', 'display': '-ms-flexbox', 'display': 'flex'});
    })

    $('.owl-hero').owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        items: 1
    })

    let img = document.querySelectorAll('.hero .img-wrapper img');
    for (let i = 0; i < img.length; i++){
        window.addEventListener('mousemove', function(e) {
            let x = e.clientX / window.innerWidth;
            let y = e.clientY / window.innerHeight;
            img[i].style.transform = 'translate(-' + x * 180 + 'px, -' + y * 100 + 'px)';
        });
    }
})
