jQuery(document).ready(function ($) {

    const slider = jQuery("#slider").owlCarousel({
        loop: true,
        dots: false,
        margin: 0,
        autoHeight: true,
        smartSpeed: 250,
        responsive: {
            0: {
                items: 1
            }
        }
    });

    const slider1 = jQuery("#slider-1").owlCarousel({
        loop: true,
        dots: false,
        margin: 0,
        autoHeight: true,
        smartSpeed: 250,
        responsive: {
            0: {
                items: 1
            }
        }
    });
    const slider2 = jQuery("#slider-2").owlCarousel({
        loop: true,
        dots: false,
        margin: 0,
        autoHeight: true,
        smartSpeed: 250,
        responsive: {
            0: {
                items: 1
            }
        }
    });

    let owl = jQuery("#slider");
    owl.owlCarousel();
    let owl1 = jQuery("#slider-1");
    owl1.owlCarousel();
    let owl2 = jQuery("#slider-2");
    owl2.owlCarousel();

    jQuery("#slider .arrow-left ").click(function () {
        owl.trigger("prev.owl.carousel");
    })

    jQuery("#slider .arrow-right").click(function () {
        owl.trigger("next.owl.carousel");
    })
    jQuery("#slider-1 .arrow-left ").click(function () {
        owl1.trigger("prev.owl.carousel");
    })

    jQuery("#slider-1 .arrow-right").click(function () {
        owl1.trigger("next.owl.carousel");
    })
    jQuery("#slider-2 .arrow-left ").click(function () {
        owl2.trigger("prev.owl.carousel");
    })

    jQuery("#slider-2 .arrow-right").click(function () {
        owl2.trigger("next.owl.carousel");
    })
});