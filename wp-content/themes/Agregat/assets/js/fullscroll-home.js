jQuery(document).ready(function ($){
    new fullpage ('#fullpage', {
        scrollHorizontally: false,
        keyboardScrolling: true,
        scrollOverflow: true,
        onLeave: function(section, origin) {
            if ((section.index === 0) && (origin.index === 1)) {
                $('header').addClass('removed');
            }
            if ((section.index === 1) && (origin.index === 0)) {
                $('header').removeClass('removed');
            }
        },
        onLeave: (origin, destination, direction) => {
            const section = destination.item;
            console.log(section);
            const titleLeft = section.querySelectorAll('.animate-left');
            const titleRight = section.querySelectorAll('.animate-right');
            const title = section.querySelectorAll('.animate');
            const tl = new TimelineMax({delay: 0.25});
            tl.fromTo(titleLeft, 0.5, { x: "0", opacity: 0 }, { y: 0, opacity: 1 });
            tl.fromTo(titleRight, 0.5, { x: "0", opacity: 0 }, { y: 0, opacity: 1 });
            tl.fromTo(title, 0.5, { y: "0", opacity: 0 }, { y: 0, opacity: 1 });
        },
    });
})