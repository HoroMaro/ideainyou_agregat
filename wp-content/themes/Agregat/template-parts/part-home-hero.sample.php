<?php
$heroBlock = get_field('hero');
$k = 0;
$activeTheme = (count($heroBlock)) ? reset($heroBlock)['select_theme']: '';
?>

<?php if (!empty($heroBlock)): ?>
<section id="page-one" class="hero<?= ' theme-' . $activeTheme; ?> section">
    <div class="layer">
        <svg width="832" height="804" viewBox="0 0 832 804" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clip-path="url(#clip0_1_4420)">
                <path d="M-69.3836 2.06934L-500.93 803.896H832L403.558 2.06934H-69.3836ZM259.709 569.039H75.5001L-41.4418 659.05L154.151 269H176.919L374.582 659.05L259.709 569.039Z" fill="#1B110A" fill-opacity="0.12"/>
            </g>
            <defs>
                <clipPath id="clip0_1_4420">
                    <rect width="1335" height="807" fill="white" transform="translate(-503)"/>
                </clipPath>
            </defs>
        </svg>

    </div>

    <?php get_template_part('template-parts/part', 'hero-socials'); ?>

    <div class="wrapper">
        <div class="left">
            <?php foreach ($heroBlock as $item): ?>
                <div class="main-content <?= 'theme-' . $item['select_theme'] ?>">
                    <?= $item['main_title'] ?>
                    <div class="img-wrapper">
                        <img src="<?= $item['image'] ?>" alt="">
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="right">
        <div class="blur"></div>
        <div class="logo">
            <a href="<?= getSiteUrl() ?>"><?= get_template_part('/template-parts/part', 'logo') ?></a>
        </div>
        <nav>
            <?php foreach ($heroBlock as $item): $k++; ?>
                <a class="<?php if ($k === 1) echo 'active'; ?>" data-theme="<?= 'theme-' . $item['select_theme'] ?>" href=""><?= $item['menu_point_title'] ?></a>
            <?php endforeach; ?>
        </nav>
        <?php foreach ($heroBlock as $item): ?>
            <div class="description <?= 'theme-' . $item['select_theme'] ?>">
                <?= $item['point_text_content_title'] ?>
                <p class="text-content"><?= $item['point_text_content_sub_title'] ?></p>
                <section class="links">
                    <div class="link-effect-2">
                        <a href="<?= $item['link']['url'] ?>" class="btn-effect" target="<?= $item['link']['target'] ?>"><span data-hover="<?= $item['link']['title'] ?>"><?= $item['link']['title'] ?></span></a>
                    </div>
                </section>
            </div>
        <?php endforeach; ?>
    </div>
</section>
<?php endif; ?>