<?php
$cardBlock = get_field('card_sections');
var_dump($cardBlock);
?>

<section class="cards">
    <div class="owl-carousel owl-theme owl-loaded">
        <div class="owl-stage-outer">
            <div class="owl-stage">
                <?php foreach ($cardBlock as $v): ?>
                    <div class="owl-item" <?php if (!empty($v['background_image'])) {echo 'style="background-image: url('. $v['background_image'] .')"';} ?>>
                        <p><?= $v['text_content'] ?></p>
                        <div>
                            <p><?= $v['day'] ?></p>
                            <p><?= $v['month'] ?><br><?= $v['year'] ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="owl-nav">
            <div class="owl-prev"><</div>
            <div class="owl-next"></div>
        </div>
    </div>
</section>
