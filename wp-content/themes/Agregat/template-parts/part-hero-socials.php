<?php $socialBlock = get_field('header_opened_' . getLang(), 'option'); ?>
<div class="socials">
    <p><?= get_field('left_side_text'); ?></p>
    <div>
        <a href="<?= getSiteLangUrl() ?>" class="lang"><?= $socialBlock['switcher_content']; ?></a>
        <?php foreach (get_field('socials', 'option') as $item): ?>
            <a href="<?= $item['link']['url'] ?>" target="<?= $item['link']['target'] ?>" class="social">
                <?= get_template_part('/template-parts/part', $item['choose_social'] . '-logo'); ?>
            </a>
        <?php endforeach; ?>
    </div>
</div>