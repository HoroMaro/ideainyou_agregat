<?php
$contentBlock = get_field('main_content_section');
?>
<?php if (!empty($contentBlock)): ?>
<section class="example-part">
    <?= $contentBlock['text_content'] ?>
    <img src="<?= $contentBlock['image'] ?>" alt="">
</section>
<?php endif; ?>