<?php
function getLang()
{
    if (function_exists('pll_current_language')) {
        return pll_current_language('slug');
    }

    return '';
}

function getSiteUrl() {
    $url = get_site_url();
    if (getLang() === 'en') {
        $url .= '/'.getLang();
    }

    return $url;
}

function getSiteLangUrl() {
    $url = get_permalink();
    if (getLang() === 'en') {
        $url = str_replace('en', 'ru', $url);
    } else {
        $url = str_replace('ru', 'en', $url);
    }

    return $url;
}