<?php
if (function_exists('acf_add_options_page')) {
    acf_add_options_page('General');
    acf_add_options_page('Menu');

    add_action('wp_header', 'headerScripts');
    add_action('wp_footer', 'footerScripts');
}

function headerScripts() {
    echo get_field('header_scripts', 'option');
}

function footerScripts() {
    echo get_field('footer_scripts', 'option');
}