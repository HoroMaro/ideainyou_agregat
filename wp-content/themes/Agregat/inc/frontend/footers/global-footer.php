<?php wp_footer(); ?>
<!--Footer Scripts-->
<?php
if (getLang()) {
    $footerScripts = get_field('scripts_'.getLang().'', 'option');
    echo $footerScripts['footer_scripts'];
}
?>

</body>
</html>