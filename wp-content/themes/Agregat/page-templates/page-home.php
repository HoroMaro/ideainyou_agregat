<?php
/* Template Name: Home */
?>

<?php get_header(); ?>

<?php wp_enqueue_script('time-line', LIB_PATH . '/TimelineMax.min.js'); ?>
<?php wp_enqueue_script('tween', LIB_PATH . '/TweenMax.min.js'); ?>
<?php wp_enqueue_style('fullview-css', CSS_PATH . '/lib/fullpage.css'); ?>
<?php wp_enqueue_style('owl', CSS_PATH . '/lib/owl.carousel.min.css'); ?>
<?php wp_enqueue_script('owl', JS_PATH . '/lib/owl.carousel.min.js', '1.0.0', true); ?>
<?php wp_enqueue_script('home-hero', JS_PATH . '/home-hero.js', '1.0.0', true); ?>
<?php wp_enqueue_script('slider-card', JS_PATH . '/home-slider-card.js', '1.0.0', true); ?>
<?php wp_enqueue_script('fullview-js', LIB_PATH . '/fullpage.js'); ?>
<?php wp_enqueue_script('fullscroll', JS_PATH . '/fullscroll-home.js', '1.0.0', true); ?>

<div id="fullpage">

    <?php get_template_part('/template-parts/part', 'home-hero'); ?>

    <?php
    $secondBlock = get_field('second_section');
    ?>
    <?php if (!empty($secondBlock)): ?>
        <section id="first-sentence" class="section">
            <div class="double-border"></div>
            <div class="main-wrapper">
                <p class="animate-left"><?= $secondBlock['top_text'] ?></p>
                <p class="animate-right"><?= $secondBlock['middle_text'] ?></p>
                <p class="animate"><?= $secondBlock['bottom_text'] ?></p>
            </div>
        </section>
    <?php endif; ?>

    <?php
    $thirdBlock = get_field('third_section');
    $thirdContent = $thirdBlock['content'];
    ?>
    <section id="facts" class="section">
        <div class="layer"></div>
        <div class="double-border"></div>
        <div class="main-wrapper">
            <div class="title animate-left">
                <?= $thirdBlock['title'] ?>
            </div>
            <ul class="items">
                <?php foreach ($thirdContent as $item): ?>
                    <li class="item animate">
                        <div class="dot"></div>
                        <div class="li-content">
                            <?= $item['title'] ?>
                            <?= $item['item_content'] ?>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </section>


    <?php
    $fourthBlock = get_field('fourth_section');
    if (!empty($fourthBlock)): ?>
        <section class="fourth-section section">
            <div class="double-border"></div>
            <div class="main-wrapper animate-left">
                <?= $fourthBlock; ?>
            </div>
        </section>
    <?php endif; ?>

    <?php
    $mapBlock = get_field('map_section');
    ?>
    <section id="map" class="section">
        <div class="double-border"></div>
        <div class="map-wrapper">
            <div class="map-content animate" style="background-image: url('<?= IMG_PATH . '/map1.png' ?>')">
                <?php
                if (getLang() == 'en') {
                    $cities = ['Hungary', 'Serbia', 'Moscow', 'Ulyanovsk'];
                } else {
                    $cities = ['Венгрия', 'Сербия', 'Москва', 'Ульяновск'];
                }
                $index = 1;
                ?>
                <!--                --><?php //foreach ($cities as $value): ?>
                <!--                    <p class="city c--><? //= $index++ ?><!--">--><? //= $value ?><!--</p>-->
                <!--                --><?php //endforeach; ?>
            </div>
        </div>
        <div class="main-wrapper animate-left">
            <div class="title">
                <div class="title-general"><?= $mapBlock['title'] ?></div>
                <div class="title-subtitle"><?= $mapBlock['sub_title'] ?></div>
            </div>
            <p class="description"><?= $mapBlock['content'] ?></p>
        </div>
    </section>
    <section class="sixth-section section">
        <div class="main-wrapper">
            <div class="title animate-left">
                <?= get_field('sixth_section'); ?>
            </div>
            <div class="subtitle animate">
                <?= get_field('sixth_section_subtitle'); ?>
            </div>
        </div>
    </section>
    <?php
    $seventhSection = get_field('seventh_section');
    $seventhButton = $seventhSection['link'];
    ?>
    <section class="img-slider section">
        <div class="double-border"></div>
        <div class="content">
            <img class="photo photo1 animate" src="<?= IMG_PATH . '/photo1.png' ?>" alt="">
            <img class="photo photo2 animate" src="<?= IMG_PATH . '/photo2.png' ?>" alt="">
            <img class="photo photo3 animate" src="<?= IMG_PATH . '/photo3.png' ?>" alt="">
            <img class="photo photo4 animate" src="<?= IMG_PATH . '/photo4.png' ?>" alt="">
            <img class="photo photo5 animate" src="<?= IMG_PATH . '/photo5.png' ?>" alt="">
            <img class="photo photo6 animate" src="<?= IMG_PATH . '/photo6.png' ?>" alt="">
            <img class="photo photo7 animate" src="<?= IMG_PATH . '/photo7.png' ?>" alt="">
            <img class="photo photo8 animate" src="<?= IMG_PATH . '/photo8.png' ?>" alt="">
            <img class="photo photo9 animate" src="<?= IMG_PATH . '/photo9.png' ?>" alt="">
            <img class="photo photo10 animate" src="<?= IMG_PATH . '/photo10.png' ?>" alt="">
            <img class="photo photo11 animate" src="<?= IMG_PATH . '/photo11.png' ?>" alt="">
            <img class="photo photo12 animate" src="<?= IMG_PATH . '/photo12.png' ?>" alt="">
            <div class="content-main animate-left">
                <div class="text">
                    <?= $seventhSection['title'] ?>
                </div>
                <section class="links">
                    <div class="link-effect-2">
                        <a href="<?= $seventhSection['link']['url'] ?>" class="btn-effect"
                           target="<?= $seventhSection['link']['target'] ?>"><span
                                    data-hover="<?= $seventhSection['link']['title'] ?>"><?= $seventhSection['link']['title'] ?></span></a>
                    </div>
                </section>
            </div>
        </div>
        <div class="content-mobile">
            <div class="text">
                <?= $seventhSection['title'] ?>
            </div>
            <div class="nav">
                <div class="prev"><?php get_template_part('/template-parts/prev', 'btn') ?></div>
                <div class="line-buf">
                    <div class="line"></div>
                </div>
                <div class="next"><?php get_template_part('/template-parts/next', 'btn') ?></div>
            </div>
            <div class="owl-carousel owl-theme">
                <div class="photo-item" style="background-image: url('<?= IMG_PATH . '/photo1.png' ?>')"></div>
                <div class="photo-item" style="background-image: url('<?= IMG_PATH . '/photo2.png' ?>')"></div>
                <div class="photo-item" style="background-image: url('<?= IMG_PATH . '/photo3.png' ?>')"></div>
                <div class="photo-item" style="background-image: url('<?= IMG_PATH . '/photo4.png' ?>')"></div>
                <div class="photo-item" style="background-image: url('<?= IMG_PATH . '/photo5.png' ?>')"></div>
                <div class="photo-item" style="background-image: url('<?= IMG_PATH . '/photo6.png' ?>')"></div>
                <div class="photo-item" style="background-image: url('<?= IMG_PATH . '/photo7.png' ?>')"></div>
                <div class="photo-item" style="background-image: url('<?= IMG_PATH . '/photo8.png' ?>')"></div>
                <div class="photo-item" style="background-image: url('<?= IMG_PATH . '/photo9.png' ?>')"></div>
                <div class="photo-item" style="background-image: url('<?= IMG_PATH . '/photo10.png' ?>')"></div>
                <div class="photo-item" style="background-image: url('<?= IMG_PATH . '/photo11.png' ?>')"></div>
                <div class="photo-item" style="background-image: url('<?= IMG_PATH . '/photo12.png' ?>')"></div>
            </div>
            <section class="links">
                <div class="link-effect-2">
                    <a href="<?= $seventhSection['link']['url'] ?>" class="btn-effect"
                       target="<?= $seventhSection['link']['target'] ?>"><span
                                data-hover="<?= $seventhSection['link']['title'] ?>"><?= $seventhSection['link']['title'] ?></span></a>
                </div>
            </section>
        </div>
    </section>

    <section class="sixth-section section">
        <div class="main-wrapper">
            <div class="title animate-left">
                <?= get_field('eight_section'); ?>
            </div>
            <div class="subtitle animate">
                <?= get_field('eight_section_subtitle'); ?>
            </div>
        </div>
    </section>

    <?php
    $productsBlock = get_field('products_baners');
    $productPassangers = $productsBlock['passangers'];
    $passagersRepeater = $productPassangers['banner_items'];
    $productDrivers = $productsBlock['drivers'];
    $driversRepeater = $productDrivers['banner_items'];
    $productTrain = $productsBlock['train'];
    $trainRepeater = $productTrain['banner_items'];
    $productAviation = $productsBlock['aviation'];
    $aviationRepeater = $productAviation['banner_items'];
    ?>
<section class="main-products section">

    <section class="products">
        <div class="double-border"></div>
        <div id="passangers">
            <div class="owl-carousel owl-theme">
                <?php foreach ($passagersRepeater as $item): ?>
                    <div class="item" style="background-image: url('<?= $item['photo'] ?>')"></div>
                <?php endforeach; ?>
            </div>
            <div class="buf-img">
                <div></div>
            </div>
            <div class="nav tablet">
                <div class="prev"><?php get_template_part('/template-parts/prev', 'btn') ?></div>
                <div class="line-buf">
                    <div class="line"></div>
                </div>
                <div class="next"><?php get_template_part('/template-parts/next', 'btn') ?></div>
            </div>
            <p class="description"><?= $productPassangers['description'] ?></p>
            <div class="main-wrapper">
                <div class="nav">
                    <div class="prev"><?php get_template_part('/template-parts/prev', 'btn') ?></div>
                    <div class="line-buf">
                        <div class="line"></div>
                    </div>
                    <div class="next"><?php get_template_part('/template-parts/next', 'btn') ?></div>
                </div>
                <div class="title">
                    <?= $productPassangers['title'] ?>
                </div>
                <p class="description mobile"><?= $productPassangers['description'] ?></p>
                <section class="links">
                    <div class="link-effect-2">
                        <a href="<?= $productPassangers['link']['url'] ?>" class="btn-effect"
                           target="<?= $productPassangers['link']['target'] ?>"><span
                                    data-hover="<?= $productPassangers['link']['title'] ?>"><?= $productPassangers['link']['title'] ?></span></a>
                    </div>
                </section>
            </div>
        </div>
    </section>

    <section class="products">
        <div class="double-border"></div>
        <div id="drivers">
            <div class="layer"></div>
            <div class="main-wrapper">
                <div class="nav tablet">
                    <div class="prev"><?php get_template_part('/template-parts/prev', 'btn') ?></div>
                    <div class="line-buf">
                        <div class="line"></div>
                    </div>
                    <div class="next"><?php get_template_part('/template-parts/next', 'btn') ?></div>
                </div>
                <div class="left-desc">
                    <div class="title">
                        <?= $productDrivers['title'] ?>
                    </div>
                    <div class="nav">
                        <div class="prev"><?php get_template_part('/template-parts/prev', 'btn') ?></div>
                        <div class="line-buf">
                            <div class="line"></div>
                        </div>
                        <div class="next"><?php get_template_part('/template-parts/next', 'btn') ?></div>
                    </div>
                </div>
                <div class="owl-carousel owl-theme">
                    <?php foreach ($driversRepeater as $item): ?>
                        <img src="<?= $item['photo'] ?>" alt="">
                    <?php endforeach; ?>
                </div>
                <div class="right-desc">
                    <p class="description"><?= $productDrivers['description'] ?></p>
                    <section class="links">
                        <div class="link-effect-2">
                            <a href="<?= $productDrivers['link']['url'] ?>" class="btn-effect"
                               target="<?= $productDrivers['link']['target'] ?>"><span
                                        data-hover="<?= $productDrivers['link']['title'] ?>"><?= $productDrivers['link']['title'] ?></span></a>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

    <section class="products block">
        <div class="double-border"></div>
        <div id="train">
            <div class="owl-carousel owl-theme">
                <?php foreach ($trainRepeater as $item): ?>
                    <div class="item" style="background-image: url('<?= $item['photo'] ?>')"></div>
                <?php endforeach; ?>
            </div>
            <div class="main-wrapper">
                <div class="left-desc">
                    <div class="title">
                        <?= $productTrain['title'] ?>
                    </div>
                    <div class="nav">
                        <div class="prev"><?php get_template_part('/template-parts/prev', 'btn') ?></div>
                        <div class="line-buf">
                            <div class="line"></div>
                        </div>
                        <div class="next"><?php get_template_part('/template-parts/next', 'btn') ?></div>
                    </div>
                </div>
                <div class="right-desc">
                    <p class="description"><?= $productTrain['description'] ?></p>
                    <section class="links">
                        <div class="link-effect-2">
                            <a href="<?= $productTrain['link']['url'] ?>" class="btn-effect"
                               target="<?= $productTrain['link']['target'] ?>"><span
                                        data-hover="<?= $productTrain['link']['title'] ?>"><?= $productTrain['link']['title'] ?></span></a>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>

    <section class="products">
        <div class="double-border"></div>
        <div id="passangers">
            <div class="owl-carousel owl-theme">
                <?php foreach ($aviationRepeater as $item): ?>
                    <div class="item" style="background-image: url('<?= $item['photo'] ?>')"></div>
                <?php endforeach; ?>
            </div>
            <div class="buf-img aviation">
                <div></div>
            </div>
            <div class="nav tablet">
                <div class="prev"><?php get_template_part('/template-parts/prev', 'btn') ?></div>
                <div class="line-buf">
                    <div class="line"></div>
                </div>
                <div class="next"><?php get_template_part('/template-parts/next', 'btn') ?></div>
            </div>
            <p class="description"><?= $productAviation['description'] ?></p>
            <div class="main-wrapper">
                <div class="nav">
                    <div class="prev"><?php get_template_part('/template-parts/prev', 'btn') ?></div>
                    <div class="line-buf">
                        <div class="line"></div>
                    </div>
                    <div class="next"><?php get_template_part('/template-parts/next', 'btn') ?></div>
                </div>
                <div class="title">
                    <?= $productAviation['title'] ?>
                </div>
                <p class="description mobile"><?= $productAviation['description'] ?></p>
                <section class="links">
                    <div class="link-effect-2">
                        <a href="<?= $productAviation['link']['url'] ?>" class="btn-effect"
                           target="<?= $productAviation['link']['target'] ?>"><span
                                    data-hover="<?= $productAviation['link']['title'] ?>"><?= $productAviation['link']['title'] ?></span></a>
                    </div>
                </section>
            </div>
        </div>
    </section>

    <section class="news">
        <div class="double-border"></div>
        <div class="main-wrapper">
            <div class="title animate-left">
                <?= get_field('read_news') ?>
            </div>
            <div class="subtitle animate">
                <?= get_field('read_news_subtitle') ?>
            </div>
        </div>
    </section>

    <?php
    $newsCards = get_field('card_section');
    if (!empty($newsCards)):
        ?>
        <section id="news">
            <div class="double-border"></div>
            <div class="layer-news">
                <svg width="832" height="804" viewBox="0 0 832 804" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0_1_4420)">
                        <path d="M-69.3836 2.06934L-500.93 803.896H832L403.558 2.06934H-69.3836ZM259.709 569.039H75.5001L-41.4418 659.05L154.151 269H176.919L374.582 659.05L259.709 569.039Z"
                              fill="#1B110A" fill-opacity="0.12"/>
                    </g>
                    <defs>
                        <clipPath id="clip0_1_4420">
                            <rect width="1335" height="807" fill="white" transform="translate(-503)"/>
                        </clipPath>
                    </defs>
                </svg>
            </div>
            <div class="main-wrapper">
                <div class="owl-carousel owl-theme">
                    <?php foreach ($newsCards as $item): ?>
                        <div class="card animate-left" style="background: #333333;">
                            <div class="image" style="background-image: url('<?= $item['background_image'] ?>')"></div>
                            <div class="layer"></div>
                            <div class="content">
                                <p><?= $item['text_content'] ?></p>
                            </div>
                            <div class="date">
                                <p><?= $item['day'] ?></p>
                                <div>
                                    <p><?= $item['month'] ?></p>
                                    <p><?= $item['year'] ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="nav animate">
                    <div class="prev"><?php get_template_part('/template-parts/prev', 'btn') ?></div>
                    <div class="line-buf">
                        <div class="line"></div>
                    </div>
                    <div class="next"><?php get_template_part('/template-parts/next', 'btn') ?></div>
                </div>
            </div>
        </section>
    <?php endif; ?>
</section>

    <?php get_footer(); ?>

</div>
