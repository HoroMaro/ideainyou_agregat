<?php
/* Template Name: Products */
?>

<?php get_header(); ?>

<?php wp_enqueue_style('owl', CSS_PATH . '/lib/owl.carousel.min.css'); ?>
<?php wp_enqueue_script('owl', JS_PATH . '/lib/owl.carousel.min.js', '1.0.0', true); ?>
<?php wp_enqueue_script('slider', JS_PATH . '/products-slider.js'); ?>

<?php
$firstProduct = get_field('products_first');

global $wp;

$link = home_url($wp->request);

?>

    <section class="first">

        <?php get_template_part('template-parts/part', 'hero-socials'); ?>

        <div class="letter"></div>
        <div class="main-wrapper">
            <div class="flex-block">
                <?= $firstProduct['main_title'] ?>
            </div>
        </div>
        <div class="buttons">
            <?php foreach ($firstProduct['buttons'] as $item): ?>
                <a href="<?= $item['link'] ?>">
                    <?php
                    if ($item['link'] == $link) {
                        $class = 'active';
                    } else {
                        $class = '';
                    }
                    ?>
                    <div class="trigger-btn <?= $class ?>">
                        <div class="grid"></div>
                        <img src="<?= $item['image'] ?>" alt="">
                        <?= $item['text'] ?>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
        <div class="logo">
            <a href="<?= getSiteUrl() ?>"><?= get_template_part('/template-parts/part', 'logo') ?></a>
        </div>
    </section>

<?php
$sliderProduct = $firstProduct['products_slider'];
$sliderCountTop = count($sliderProduct['sliders_top']);
$sliderCountMiddle = count($sliderProduct['sliders_middle']);
$sliderCountBottom = count($sliderProduct['sliders_bottom']);
$sliderCountTopStart = count($sliderProduct['sliders_top']);
$sliderCountMiddleStart = count($sliderProduct['sliders_middle']);
$sliderCountBottomStart = count($sliderProduct['sliders_bottom']);

?>

<?php
$tCounter = 0;
?>
    <section class="trigger<?php if ($tCounter === 0) {
        echo ' show';
    } ?>">
        <section class="slider-info">
            <div class="main-img">
                <div class="img-center">
                    <div class="grid"></div>
                    <img src="<?= $sliderProduct['back_photo'] ?>" alt="">
                </div>
            </div>
            <div class="main-wrapper">
                <div class="flex-block">
                    <div>
                        <?= $sliderProduct['main_title'] ?>
                    </div>
                    <div>
                        <p>
                            <?= $sliderProduct['description'] ?>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="slider-content">
            <div class="owl-carousel slider" id="slider">
                <?php foreach ($sliderProduct['sliders_top'] as $index => $item): ?>
                    <div class="content theme-<?= $item['theme'] ?>">
                        <div class="back left"></div>
                        <div class="back right"></div>
                        <div class="main-wrapper">
                            <div class="flex-block">
                                <div class="back"></div>
                                <?php if ($sliderCountTopStart > 1): ?>
                                    <div class="back">
                                        <div class="slide-arrow">
                                            <div class="arrow-left"></div>
                                            <div class="line">
                                                <div class="line-color"
                                                     style="width: <?= $sliderCountTop ? round(100 / $sliderCountTop) : 0; ?>%"></div>
                                                <?php $sliderCountTop -= 1 ?>
                                            </div>
                                            <div class="arrow-right"></div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <img src="<?= $item['photo'] ?>" alt="">
                                <?php $count = 1; ?>
                                <?php foreach ($item['hints'] as $item2): ?>
                                    <div class="dot dot-<?= $count; ?>">
                                        <span><?= $item2['text'] ?></span>
                                    </div>
                                    <?php $count++; ?>
                                <?php endforeach; ?>
                                <div class="top">
                                    <div class="left">
                                        <?php $buttonLink = $item['link_text'] ?>
                                        <?= $item['title'] ?>
                                        <?php if (!empty($buttonLink)): ?>
                                            <a href="<?= $buttonLink['url'] ?>"
                                               target="<?= $buttonLink['target'] ?>"><?= $buttonLink['title'] ?></a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="right">
                                        <?= $item['description'] ?>
                                    </div>
                                </div>
                                <div class="bottom">
                                    <p class="description"><?= $item['subdescription'] ?></p>
                                    <div class="items">
                                        <?php $count = 1; ?>
                                        <?php foreach ($item['modifications'] as $item2): ?>
                                            <div>
                                                <h3><?= $item2["title"] ?></h3>
                                                <p class="text"><?= $item2["text"] ?></p>
                                                <p><?= str_pad($count, 2, 0, STR_PAD_LEFT) ?></p>
                                            </div>
                                            <?php $count++; ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </section>
        <section class="slider-content">
            <div class="owl-carousel slider" id="slider-1">
                <?php foreach ($sliderProduct['sliders_middle'] as $index => $item): ?>
                    <div class="content theme-<?= $item['theme'] ?>">
                        <div class="back left"></div>
                        <div class="back right"></div>
                        <div class="main-wrapper">
                            <div class="flex-block">
                                <div class="back"></div>
                                <?php if ($sliderCountMiddleStart > 1): ?>
                                    <div class="back">
                                        <div class="slide-arrow">
                                            <div class="arrow-left"></div>
                                            <div class="line">
                                                <div class="line-color"
                                                     style="width: <?= $sliderCountMiddle ? round(100 / $sliderCountMiddle) : 0; ?>%"></div>
                                                <?php $sliderCountMiddle -= 1 ?>
                                            </div>
                                            <div class="arrow-right"></div>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <img src="<?= $item['photo'] ?>" alt="">
                                <?php $count = 1; ?>
                                <?php foreach ($item['hints'] as $item2): ?>
                                    <div class="dot dot-<?= $count; ?>">
                                        <span><?= $item2['text'] ?></span>
                                    </div>
                                    <?php $count++; ?>
                                <?php endforeach; ?>
                                <div class="top">
                                    <div class="left">
                                        <?php $buttonLink = $item['link_text'] ?>
                                        <?= $item['title'] ?>
                                        <?php if (!empty($buttonLink)): ?>
                                            <a href="<?= $buttonLink['url'] ?>"
                                               target="<?= $buttonLink['target'] ?>"><?= $buttonLink['title'] ?></a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="right">
                                        <?= $item['description'] ?>
                                    </div>
                                </div>
                                <div class="bottom">
                                    <p class="description"><?= $item['subdescription'] ?></p>
                                    <div class="items">
                                        <?php $count = 1; ?>
                                        <?php foreach ($item['modifications'] as $item2): ?>
                                            <div>
                                                <h3><?= $item2["title"] ?></h3>
                                                <p class="text"><?= $item2["text"] ?></p>
                                                <p><?= str_pad($count, 2, 0, STR_PAD_LEFT) ?></p>
                                            </div>
                                            <?php $count++; ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </section>
    </section>

    <section class="slider-content">
        <div class="owl-carousel slider" id="slider-2">
            <?php foreach ($sliderProduct['sliders_bottom'] as $index => $item): ?>
                <div class="content theme-<?= $item['theme'] ?>">
                    <div class="back left"></div>
                    <div class="back right"></div>
                    <div class="main-wrapper">
                        <div class="flex-block">
                            <div class="back"></div>
                            <?php if ($sliderCountBottomStart > 1): ?>
                                <div class="back">
                                    <div class="slide-arrow">
                                        <div class="arrow-left"></div>
                                        <div class="line">
                                            <div class="line-color"
                                                 style="width: <?= $sliderCountBottom ? round(100 / $sliderCountBottom) : 0; ?>%"></div>
                                            <?php $sliderCountBottom -= 1 ?>
                                        </div>
                                        <div class="arrow-right"></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <img src="<?= $item['photo'] ?>" alt="">
                            <?php $count = 1; ?>
                            <?php foreach ($item['hints'] as $item2): ?>
                                <div class="dot dot-<?= $count; ?>">
                                    <span><?= $item2['text'] ?></span>
                                </div>
                                <?php $count++; ?>
                            <?php endforeach; ?>
                            <div class="top">
                                <div class="left">
                                    <?php $buttonLink = $item['link_text'] ?>
                                    <?= $item['title'] ?>
                                    <?php if (!empty($buttonLink)): ?>
                                        <a href="<?= $buttonLink['url'] ?>"
                                           target="<?= $buttonLink['target'] ?>"><?= $buttonLink['title'] ?></a>
                                    <?php endif; ?>
                                </div>
                                <div class="right">
                                    <?= $item['description'] ?>
                                </div>
                            </div>
                            <div class="bottom">
                                <p class="description"><?= $item['subdescription'] ?></p>
                                <div class="items">
                                    <?php $count = 1; ?>
                                    <?php foreach ($item['modifications'] as $item2): ?>
                                        <div>
                                            <h3><?= $item2["title"] ?></h3>
                                            <p class="text"><?= $item2["text"] ?></p>
                                            <p><?= str_pad($count, 2, 0, STR_PAD_LEFT) ?></p>
                                        </div>
                                        <?php $count++; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php $tCounter++; ?>

    <section class="bottom-buttons">
        <div class="main-wrapper">
            <div class="flex-block">
                <div class="buttons">
                    <?php foreach ($firstProduct['buttons'] as $item): ?>
                        <div class="trigger-btn">
                            <div class="grid"></div>
                            <img src="<?= $item['image'] ?>" alt="">
                            <p class="other"><?= $item['other'] ?></p>
                            <?= $item['text'] ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>