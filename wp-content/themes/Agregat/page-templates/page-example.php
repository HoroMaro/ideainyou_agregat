<?php
/* Template Name: Example Page */
?>

<?php get_header(); ?>

<?php wp_enqueue_style('owl', CSS_PATH . '/lib/owl.carousel.min.css'); ?>
<?php wp_enqueue_script('owl', JS_PATH . '/lib/owl.carousel.min.js', '1.0.0', true); ?>

<?php
$heroBlockTitle = get_field('title');
$heroBlockImage = get_field('image');
$heroBlockButton = get_field('button');
?>
<section class="example">
    <h2><?= $heroBlockTitle ?></h2>
    <img src="<?= $heroBlockImage ?>" alt="">
    <a href="<?= $heroBlockButton['url'] ?>" target="<?= $heroBlockButton['target'] ?>"><?= $heroBlockButton['title'] ?></a>
</section>

<?php get_template_part('template-parts/part', 'example'); ?>

<?php get_footer(); ?>