<?php
/* Template Name: About */
?>
<?php get_header(); ?>

<?php wp_enqueue_style('owl', CSS_PATH . '/lib/owl.carousel.min.css'); ?>
<?php wp_enqueue_script('owl', JS_PATH . '/lib/owl.carousel.min.js', '1.0.0', true); ?>
<?php wp_enqueue_script('about-carousel', JS_PATH . '/about-carousel.js', '1.0.0', true); ?>

<?php
$heroBlock = get_field('hero');
?>
<section class="hero">
    <div class="layer-left"></div>
    <div class="double-border"></div>
    <div class="main-wrapper">
        <div class="title">
            <?= $heroBlock['title'] ?>
        </div>
        <div class="subtitle">
            <p><?=$heroBlock['subtitle']?></p>
        </div>
    </div>
</section>

<?php
$secondBlock = get_field('second-block');
?>

<?php foreach ($secondBlock as $item): ?>
    <section class="hero">
        <div class="second-left">
            <div class="subtitles">
                <p><?=$item['text-title']?></p>
                <p><?=$item['second-subtitle']?></p>
            </div>
        </div>
        <div class="main-map">
            <div class="moscow-main-map"></div>
            <div class="maps-moscow"></div>
        </div>
        <div class="double-border"></div>
        <div class="main-wrappers">
            <div class="titles">
                <?= $item['second-title'] ?>
            </div>
        </div>
    </section>
<?php endforeach; ?>
    <?php
    $factsContainer = get_field('factsContainer');
    ?>
<section class="page p3 back-black">
    <div class="layer-left"></div>
    <div class="double-border"></div>
    <div class="content">
        <p class="text">
            <span class="border">All of us</span><br>
            <span class="bold">love the <span class="red">facts</span></span>
        </p>
        <ul class="items">
            <li class="item">2008
                <p class="description">year <span class="white">of foundation</span> with current shareholders</p>
            </li>
            <li class="item">302
                <p class="description">the <span class="white">number of employees</span> in the entire group of companies</p>
            </li>
            <li class="item">€ 10+ mln
                <p class="description">is the <span class="white">group revenue</span> for 2020</p>
            </li>
            <li class="item">12 000m²
                <p class="description">production <span class="white">area</span></p>
            </li>
        </ul>
    </div>
    <div class="suka">
        <div class="photoh">
            <img class="palka" src="<?= IMG_PATH . '/Mask Group.png' ?>" alt="">
         </div>
    </div>


</section>
<?php
$redPassion = get_field('redPassion');
?>
<section class="red-passion">
    <div class="layer-passion">
        <div class="double-border"></div>
        <div class="main-wrapper">
            <div class="title-passion">
                <?= $redPassion['titleredpassion'] ?>
            </div>
        </div>
    </div>
</section>
<section class="page p7 back-grey2">
    <div class="double-border"></div>
    <div class="content">
        <img class="photo photo1" src="<?= IMG_PATH . '/photo1.png' ?>" alt="">
        <img class="photo photo2" src="<?= IMG_PATH . '/photo2.png' ?>" alt="">
        <img class="photo photo3" src="<?= IMG_PATH . '/photo3.png' ?>" alt="">
        <img class="photo photo4" src="<?= IMG_PATH . '/photo4.png' ?>" alt="">
        <img class="photo photo5" src="<?= IMG_PATH . '/photo5.png' ?>" alt="">
        <img class="photo photo6" src="<?= IMG_PATH . '/photo6.png' ?>" alt="">
        <img class="photo photo7" src="<?= IMG_PATH . '/photo7.png' ?>" alt="">
        <img class="photo photo8" src="<?= IMG_PATH . '/photo8.png' ?>" alt="">
        <img class="photo photo9" src="<?= IMG_PATH . '/photo9.png' ?>" alt="">
        <img class="photo photo10" src="<?= IMG_PATH . '/photo10.png' ?>" alt="">
        <img class="photo photo11" src="<?= IMG_PATH . '/photo11.png' ?>" alt="">
        <img class="photo photo12" src="<?= IMG_PATH . '/photo12.png' ?>" alt="">
        <p class="text">We are the regular guest at exhibitions of <span class="white">modern technologies for public transport</span></p>
        <a href="" class="news">Go to the other news</a>
    </div>
</section>
<section class="page p8 major back-white">
    <div class="double-border"></div>
    <div class="major-man">
        <img class="photo photo1" src="<?= IMG_PATH . '/man 1.png' ?>" alt="">
    </div>
    <div class="vector-white"></div>
    <div class="main-wrapper">
        <div class="flex-block">
            <div class="content-major">
                <p class="text-major">
                    <span class="border">MAJOR</span><br>
                    <span class="bold">PEOPLE ARE<span class="red"></span></span>
                </p>
                <p class="text-mask">
                    <span class="border-mask">Chief Executive Oficcer</span><br>
                    <span class="bold-mask">Ilon Mask<span class="red"></span></span>
                </p>
                <div class="sub-text-mask">
                    <p>
                        <span class="subtitle-mask">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using <span class="red"></span></span>
                    </p>
                </div>
            </div>
            <div class="sub-text-ilon-mask">
                <p>
                    <span class="subtitle-ilon-mask">Content here, content here It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here <span class="red"></span></span>
                </p>
                <a href="" class="news">GET IN TOUCH</a>
            </div>
        </div>
    </div>
</section>
<?php
$redstructure = get_field('redstructure');
?>
<section class="red-passion">
    <div class="layer-passion">
        <div class="double-border"></div>
        <div class="main-wrapper">
            <div class="title-passion">
                <?= $redstructure['titleredStructure'] ?>
            </div>
        </div>
    </div>
</section>

<?php
$organizationBlock = get_field('organization_block');
?>

<section class="page p9 organization back-white">
    <div class="white-form"></div>
    <div class="main-wrapper">
        <div class="flex-block">
            <div class="text">
                <?= $organizationBlock["title"] ?>
            </div>
            <ul>
                <?php foreach ($organizationBlock['items'] as $item): ?>
                    <li>
                        <?= $item["item"] ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="red-talk">
        <a href="<?= $organizationBlock["link"]["url"] ?>" target="<?= $organizationBlock["link"]["target"] ?>" class="news"><?= $organizationBlock["link"]["title"] ?></a>
    </div>

</section>
<?php
$redcertificates = get_field('redcertificates');
?>
<section class="red-passion">
    <div class="layer-passion">
        <div class="double-border"></div>
        <div class="main-wrapper">
            <div class="title-passion">
                <?= $redcertificates['titleredcertificates'] ?>
            </div>
        </div>
    </div>
</section>

<section class="page p9 certificates">
    <div class="title">NEED SOME PROOFS?</div>
    <div class="content-certificates">
        <div class="about-certificates">
            <img class=certificate1 src="<?= IMG_PATH . '/Certificate1.png' ?>" alt="">
            <img class=certificate2 src="<?= IMG_PATH . '/012 1.png' ?>" alt="">
            <img class=certificate3 src="<?= IMG_PATH . '/013 1.png' ?>" alt="">
            <img class=certificate4 src="<?= IMG_PATH . '/014 1.png' ?>" alt="">
        </div>
    </div>
</section>

<section class="silver">
    <div class="red-slider">
        <a href="" class="news">Time to talk</a>
    </div>
</section>

<?php get_footer(); ?>

