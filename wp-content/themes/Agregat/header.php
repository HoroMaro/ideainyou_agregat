<?php get_template_part('inc/frontend/headers/global-header', '', (!empty($args)) ? $args : []); ?>

<?php $headerBlock = get_field('header_opened_' . getLang(), 'option') ?>
<header class="header">
    <a class="logo" href="<?= getSiteUrl() ?>"><?= get_template_part('/template-parts/part', 'logo') ?></a>
    <div class="block-menu">
        <input class="menu__toggle"  id="menu__toggle" type="checkbox" />
        <label class="menu__btn" for="menu__toggle">
            <span></span>
        </label>
        <div class="title">
            <h2 class="text"><?= get_field('header_text_' . getLang(), 'option'); ?></h2>
        </div>
        <div class="full-menu">
            <div class="logo">
                <a href="<?= getSiteUrl() ?>"><?= get_template_part('/template-parts/part', 'logo') ?></a>
            </div>
            <div class="back-image">
                <div class="filter">
                    <img src="<?= IMG_PATH . '/header_back.png' ?>" alt="">
                </div>
            </div>
            <div class="menu">
                <div class="left">
                    <div class="menu-pages">
                        <?php foreach ($headerBlock['navigation'] as $item): ?>
                            <?php (get_permalink() === $item['link']['url']) ? $active = ' active' : $active = ''; ?>
                            <a href="<?= $item['link']['url'] ?>" target="<?= $item['link']['target'] ?>" class="menu-page<?= $active ?>"><?= $item['link']['title'] ?></a>
                        <?php endforeach; ?>
                    </div>
                    <div class="contacts">
                        <?php 
                            foreach ($headerBlock['contacts'] as $item) {
                                echo '<a href="'. $item['link']['url'] .'" target="'. $item['link']['target'] .'" class="text">'. $item['link']['title'] .'</a>';
                            } 
                        ?>
                    </div>
                </div>
                <div class="right">
                    <a href="<?= getSiteLangUrl() ?>" class="lang"><?= $headerBlock['switcher_content']; ?></a>
                    <div class="socials">
                        <?php foreach (get_field('socials', 'option') as $item): ?>
                            <a href="<?= $item['link']['url'] ?>" target="<?= $item['link']['target'] ?>" class="social">
                                <?= get_template_part('/template-parts/part', $item['choose_social'] . '-logo'); ?>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>